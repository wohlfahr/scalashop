package scalashop

def howManyHoursISpentOnThisLab: Int = {
  -1
} ensuring(_ > 0)
